# AleVT

This is a program for viewing Videotext/Teletext from /dev/vbi and
/dev/dvb/adapterN/demuxN devices.

This fork of AleVT takes over from where [LinuxTV][legacy] ended. It's
forked from Edgar Törnig's last release in 2003 and incorporates
patches from Debian, LinuxTV and other sources.

 [legacy]: https://www.linuxtv.org/downloads/legacy/

# Web site

The new web site for AleVT is https://gitlab.com/alevt/alevt
