#ifndef EDIT_H
#define EDIT_H

#include "vt.h"
#include "xio.h"
#include "edline.h"

struct edwin {
    struct xio_win *xw;
    int pgno, subno;
    int x, y;		// cursor pos
    int reveal;
    int mode;		// insert mode
    struct edline *edline;
    u8 tmpbuf[40];	// to hold the bottom line while edline active
};

struct edwin *edwin_new(struct xio *xio, struct vbi *vbi, int pgno, int subno);

#endif
